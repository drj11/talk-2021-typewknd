# Script

## Timesheet

- 2m :intro: Introduction to David Jones
- 2m :app: Terminal The App
- 2m :model: The Terminal Model, challenge response
- 2m :history: Terminal History
- 2m :essential: Essential Terminal Commands
- 2m :drag: The Terminal and The Finder
- 2m :harfbuzz: using hb-view and otfinfo
- 2m :svg: using rsvg-convert
- 2m :font-bakery: using font-bakery
- 2m :ttx: using ttx
- 2m :brew: installing things with brew
- 2m :wrap-up: wrapping up and further reading


## The Big Mess

Pedagogy.
Narrative.
Arc.
Mission.
Community.


## Snippets

Maybe next TypeWknd we can do an in-depth on monospace or
duospace fonts?


## The Terminal Model

Challenge and Response.

Using Terminal is a conversation.

A conversation with two participants: one a patient and humble servant,
the other a computer filled with capricious malice.

A conversation is sometimes simple, like a sequence of instructions;
sometimes it is a discussion or a exploration;
sometimes emotions run high and there are hostilities.

The core of this conversation is a prompt, a challenge, and a response.

The prompt is issued by the computer,
it tells us that Terminal is ready.
We type, and press Return.
There are exceptions, but
generally Terminal will not do much until we press Return.
From now on i’ll probably forget to mention the Return thing,
even if i wanted to.

Mostly you can type any old rubbish and you get some response.
But sometimes the shell thinks that the command is not complete,
so pressing Return blah blah blah.
I like escaping this situation by pressing Ctrl-C,
which is the Unix interrupt.

## :intro: David Jones

Hi my name is David Jones and i'd like to tell you about the
miraculous wonders of The Terminal.
But a little about me.
Before the long year of the pandemic i was a programmer at a
university in the middle of england.
I got thoroughly disillusioned with the idea of working through
a pandemic and decided to test whether i could be a type designer.
i dusted off my 20-year old copy of Bringhurst and actually
re-read it.
In March of what is technically 2021 I bought a 2015 Mac Book Pro,
and did a short course on Glyphs Mini with The League of Movable Type.

I now have Atwin listed on MyFonts.com and a tiny foundry called
Cubic Type.

## :app: Terminal the App

Terminal is an app.
It lives in the Utilities Folder of the Application Folder,
which you can access from the Go menu of Finder.

It doesn't really follow the traditional document model,
so it doesn't have a File menu.
But it does have windows, and you can set many preferences with
Command-comma in the usual way.

Each window in Terminal is an emulation of a physical terminal.
Each window has a separate existence.
Generally, things you do in one window won't directly affect
another window;
except for the fact that it's all running on your computer and
they share a filesystem.

Typically i'll start Terminal when I boot up my Mac and never
quit until i restart.
Since i got my Mac in March, I think I've done it a couple of
times now.


## :history: Terminal History

Why is Terminal like this?

The basic paradigm:
Text, a sequence of characters, a command, a response is old, in
computer terms it’s ancient.
It is an interface that was created to be simple.
Simple to implement, simple to use.
And possible to implement on a scrounged PDP-11.

The paradigm proved flexible and has stood the test of time.
And, much to my surprise, not just favoured by crusty old men
like me.

The name Terminal comes from the fact that ancient computers
used to have a separate keyboard and screen (or printer)
connected with a cable.
The keyboard and screen was at the end of a cable,
so was called a terminal.


## :essential: Essential Terminal Commands

- pwd
- cd
- ls
- cat
- more
- nano
- man
- --help

## :drag: The Terminal and The Finder

One of the more fun things i like about Terminal is the way it
can work with the Finder and with other macOS programs.

The simplest and most universal of these is `open`.
This is a command line utility that is roughly the equivalent of
double-clicking a thing in Finder.
So, if you are in some particular directory in a Terminal
window, and you want to see it as a folder in Finder, you can
type

    open .

[followed by return]

Remember that by convention on Unix `.` refers to the current
directory.

If you have an image, you can open it in Preview using `open`:

    open hello.png

It's fun to see what works.

There are a couple of things that work from Finder.
If you Control-click on a folder and open the `Services` item on
the context menu, you should find the `New Terminal at Folder`
item.
This starts a new Terminal window that already has that Folder
set as the current directory.

Another thing you can do is drag the proxy icon into a Terminal
window.
The proxy icon is this fabulous thing that macOS has had since
the 1980s, but a lot of people have never been told about the
ancient runes.

When you open a document in macOS the window in which it appears
has a title and a proxy icon. These days the proxy icon in
sometimes hidden.

Here's an example in Finder.
Notice how i have to hover over the Window title before the
proxy icon appears.
Once the proxy icon has appeared, i can drag that, by clicking
and holding and moving the mouse, just as if it was the
document. The icon is a proxy for the document.

When i drag a proxy icon or a document into a Terminal window,
instead of Terminal opening that document, which isn't really
possible, Terminal will paste the location of that document into
the Window, as if you had typed it.

## END
