# 2021 TypeWknd 2021

## Brief

From the Call for Speakers e-mail dated 2021-06-02:

“20-minute talk.
A pre-recorded video & 10-min live Q&A chat”

(alternative is a 1-hr workshop which we have not submitted for)

“Content we're looking for:
• Appeals to a global typographic audience
• Serves an underinvested community
• Provides value to attendees from a range of experience levels
”

## Timesheet

- 2m Introduction to David Jones
- 2m Terminal The App
- 2m The Terminal Model, challenge response
- 2m Terminal History
- 2m Essential Terminal Commands
- 2m The Terminal and The Finder
- 2m using hb-view and otfinfo
- 2m using rsvg-convert
- 2m using font-bakery
- 2m using ttx
- 2m installing things with brew
- 2m wrapping up and further reading


## The Big Mess

Pedagogy.
Narrative.
Arc.
Mission.
Community.


## Bio

I have spent a long time in Programming Land.
In the long year of the pandemic, i made an extended backpacking trip into Graphic Design land.
I bought a Mac. I made tools. I made fonts.
I unearthed the fonts of ancient video-games.
I got the same pencil sharpener that Jessica Hische uses.
I read Bringhurst, Lupton, Cheng,
the cute little orange book, the OpenType specification.
I have been transported to distant places:
ANRT, Letterform Archive, Type@Cooper, The Arabic Book Cover Design Archive.
All through a 13inch sheet of glass.

I kerned. I learned. I’m here to share a little bit of code.

## Description

I live in the command line.

This talk will be a short walk-through some of the tools and techniques
that i use on the command line to
help me make fonts, choose fonts, and analyse fonts.
You should be able to follow the talk even if
you have never used the command line.
I show how i use hb-view (from harfbuzz)
to show snippets from a font and preview OpenType features;
how i use rsvg-convert and kitty to show SVG files;
how i use font-bakery to QA fonts;
how i use ttx to inspect and fix fonts;
and how i use scripting to make repeatable tests.

You should leave with a taste of what is possible using the command line,
and maybe a thirst to find out more. Should you use the command line too?
